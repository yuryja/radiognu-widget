# Widget (Responsivo) de RadioÑú

## ¿Qué es lo que hace?

Incluye (casi) todas las funcionalidades del [antiguo Widget de RadioÑú] [1],
pero con una interfaz de usuario renovada y más intuitiva (o eso esperamos).

Integra el trabajo del [Reproductor de Audio HTML5 Universal] [2].

Este widget pretender ser fácilmente agregado a cualquier sitio web,
incrustándolo en la parte de la página donde quiera que aparezca. También será
posible agregarlo más fácilmente aún usando el [módulo de django] [3] o
[el plugin de wordpress] [4]

## ¿Cómo lo puedo probar?

Por ahora lo puedes ver en acción en [radiognu.org] [5]

También puedes hacer un clon del repositorio y abrir el archivo `index.html` en
el navegador de tu preferencia.

## ¿Cómo funciona?

Puedes documentarte acerca del proyecto en su [wiki] [6] :soon:

## ¿Puedo ayudar?

Por supuesto. Si encuentras fallas, tienes sugerencias o mejoras que proponer,
puedes hacerlo en la sección de [seguimiento de incidentes] [7].

Ahora, si te sientes con ganar de aportar directamente con código, puedes clonar
el repositorio (con los botones de más arriba), y luego hacer una Peticion de
*pull* con tus cambios.

Licenciamos nuestro código bajo la [GNU GPLv3] [8], cuyo texto se encuentra en
el archivo gpl.txt.

[1]: https://gitlab.com/radiognu/radiognu-widget-legacy/
[2]: https://gitlab.com/BreadMaker/universal-html5-audio/
[3]: https://gitlab.com/radiognu/radiognu-widget/issues/9/
[4]: https://gitlab.com/radiognu/radiognu-widget/issues/8/
[5]: http://radiognu.org/
[6]: https://gitlab.com/radiognu/radiognu-widget/wiki/
[7]: https://gitlab.com/radiognu/radiognu-widget/issues/
[8]: http://www.gnu.org/licenses/gpl-3.0.html