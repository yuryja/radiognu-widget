/*
 * main.js -- Javascript para el widget del proyecto radiognu-widget
 *
 * @license Copyright 2013 - 2016 BreadMaker aka CTM <breadmaker@radiognu.org>
 *
 * Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
 * los términos de la Licencia Pública General GNU tal como se publica por
 * la Free Software Foundation; ya sea la versión 3 de la Licencia, o
 * (a su elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que le sea útil, pero SIN
 * NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
 * IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
 * General de GNU para más detalles.
 *
 * Debería haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa; de lo contrario escriba a la Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, EE. UU.
 */

var titl;

function formatTitle() {
    if ($("#wid-listeners-quantity:not(:empty)").length !== 0 && !isNaN(parseInt($(
            "#wid-listeners").data("quantity")))) document.title = parseInt($(
        "#wid-listeners").data("quantity")) + " escuchando " + titl;
}

// Verifica si un selector existe en el DOM
jQuery.fn.exists = function () {
    return this.length > 0;
};

var manualFetchTimer;

function initSocket() {
    var socket = io.connect("https://savonet-flows-socket.herokuapp.com", {
        transports: ["websocket", "htmlfile", "xhr-polling", "jsonp-polling"]
    });
    var firstConnect = true;
    socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
    socket.on('joined', function (data) {
        if (!firstConnect) getInfo({
            "artist": data.artist,
            "title": data.title
        });
    });
    socket.on('error', function (data) {
        console.debug("Ha ocurrido un error con la conexión.");
        if (manualFetchTimer === undefined) {
            manualFetchTimer = setInterval(function () {
                socket = io.connect(
                    "https://savonet-flows-socket.herokuapp.com", {
                        transports: ["websocket", "htmlfile",
                            "xhr-polling", "jsonp-polling"
                        ]
                    });
                getInfo();
            }, 30000);
        }
    });
    socket.on('disconnect', function () {
        console.debug("Se ha perdido la conexión con el socket.");
    });
    socket.on('reconnecting', function () {
        console.debug("Se ha iniciado la reconexión con el socket.");
    });
    socket.on('reconnect', function () {
        console.debug("La reconexión con el socket ha sido exitosa.");
        firstConnect = false;
        socket.emit('join', "e710da7b9e83debd5dcb4a5455e9998caba8fca7");
    });
    socket.on('reconnect_failed', function () {
        console.debug("La reconexión con el socket ha fallado.");
    });
    socket.on("e710da7b9e83debd5dcb4a5455e9998caba8fca7", function (data) {
        if (data.cmd === "metadata") {
            getInfo({
                "artist": data.radio.artist,
                "title": data.radio.title
            });
            console.debug(
                "Se han recibidos nuevos metadatos desde el servidor.");
        } else if (data.cmd === "ping radio") {
            console.debug(
                "Se he recibido una solicitud de ping desde el servidor.");
        }
    });
}

var artist, title;

function getInfo(metadata) {
    if ((navigator.hasOwnProperty("onLine") || Navigator.prototype.hasOwnProperty(
            "onLine")) && navigator.onLine) {
        if (metadata !== undefined) {
            if ($(".wid-now-playing-artista").text() != metadata.artist || $(
                    ".wid-now-playing-titulo").text() != metadata.title) {
                $("#wid-now-playing-loading").fadeIn(500);
                $.getJSON("https://radiognu.org/api/?img_size=175", metadata, function (
                    data) {
                    $("#wid-now-playing-loading").fadeOut(500);
                    if (data.artist === null && data.title === null)
                        showIcecastError();
                    else processDataFromAPI(data);
                });
            }
        } else {
            $("#wid-now-playing-loading").fadeIn(500);
            $.getJSON("https://radiognu.org/api/", {
                img_size: 175
            }, function (data) {
                $("#wid-now-playing-loading").fadeOut(500);
                if (artist != data.artist || title != data.title)
                    processDataFromAPI(data);
                else if (data.artist === null && data.title === null)
                    showIcecastError();
                else {
                    $("#wid-listeners-quantity").text(data.listeners);
                    $("#wid-listeners").data({
                        quantity: data.listeners
                    });
                }
            });
        }
    }
}

function processDataFromAPI(data) {
    if ($("#wid-loading-message").exists()) {
        isFirstTime = true;
        startWidget();
    }
    animImg(data.cover);
    animDiv(".wid-now-playing-titulo", data.title);
    setTimeout(function () {
        animDiv(".wid-now-playing-artista", data.artist);
    }, 100);
    setTimeout(function () {
        animDiv(".wid-now-playing-album", data.album);
    }, 200);
    setTimeout(function () {
        animDiv(".wid-now-playing-other", {
            "country": data.country,
            "year": data.year,
            "license": data.license
        });
    }, 300);
    $("#wid-listeners-quantity").text(data.listeners);
    $("#wid-listeners").data({
        quantity: data.listeners
    });
    artist = data.artist;
    title = data.title;
    if (data.isLive) {
        $("#wid-now-playing-live-indicator").fadeIn(500);
    } else {
        $("#wid-now-playing-live-indicator").fadeOut(500);
    }
    if (isFirstTime)
        $("iframe", window.parent.document).height($("#radiognu-widget").height());
}

function animDiv(div, info) {
    var newDiv;
    if (div == ".wid-now-playing-other") {
        newDiv = $(div + " .wid-now-playing-info-line").clone().addClass("in");
        newDiv.find(".wid-now-playing-pais").html(info.country);
        newDiv.find(".wid-now-playing-año").html(info.year);
        if (info.license === "") {
            newDiv.find(".wid-now-playing-separator:last").addClass("hide");
        } else {
            newDiv.find(".wid-now-playing-separator.hide").removeClass("hide");
        }
        newDiv.find(".wid-now-playing-licencia").removeAttr("title").html($("<a>").attr({
            href: info.license.url,
            target: "_blank",
            title: "Licencia: " + info.license.name
        }).html(info.license.shortname));
    } else {
        newDiv = $(div + " .wid-now-playing-info-line").clone().addClass("in").html(
            info);
    }
    $(div).append(newDiv).find(".wid-now-playing-info-line:first").addClass("out").emulateTransitionEnd(
        750).one("bsTransitionEnd", function () {
        $(this).remove();
    });
    setTimeout(function () {
        $(div + " .wid-now-playing-info-line.in").removeClass("in");
    }, 17); // 1000 / 60 (fps) = 16.666666666...
}

function showIcecastError() {
    if ($("#wid-loading-message").exists()) {
        $("#wid-loading-message-text").html($("<i class='icon-frown'></i>")).append(
            " Lo sentimos, no hay conexión al servidor Icecast. Inténtelo más tarde."
        );
    }
}

function errorAlbumImg() {
    if ($(".wid-np-album-img:first img").attr("src") !== "") {
        $(".wid-np-album-img:first").animate({
            "opacity": ".5"
        }, {
            duration: 500,
            queue: false
        }).css("cursor", "pointer").children().animate({
            "opacity": "1"
        }, {
            duration: 500,
            queue: false
        }).parent().off("click").click(function () {
            var s = $(".wid-np-album-img img:first").attr("src");
            $(".wid-np-album-img img:first").attr("src", "").attr("src", s);
        });
    }
}

function animImg(src) {
    if (src === undefined) src =
        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAK8AAACvCAMAAAC8TH5HAAADAFBMVEX///9VVf9UVP5YWP9WVv9aWv/9/f37+/tTU/tUVP1cXP9UVPxTU/pfX/9dXf/U1NRRUfRSUlLn5+dFRdDKyspeXv9WVf3y8vL19fVMTORJSd1FRdE6OrEfH0nf398bG1JPUP9SUvhQUPFOTuzi4uJLS+Lc3NzZ2dk8PLUhIWTW1taqqqooKHwmJnRPT09SU/9MTv/6+vru7u7p6elNTejl5eVHRtU1NaAyMpcwMJEtLYkrK4R2dnYeHlz/zABiYv9DQ8o4OKqLi4uAgIAqKn97e3ttbW0kJG0iImhjY2RaWloaGk4+Pj8TEzw4ODlPT+9KSuBISNpHR9fFxcW7u7uysrKgoKCDg4MgIGAdHVg6Ok0fH0M0NDUPDyz/0wDw8PDR0dG/v789Pbu1tbU3N6aampozM5qNjY0uLowcHFUmJko2NkYWFkMaGj4hITkoKCxBQcRAQME+Pr6lpaWdnZ00NJ2Xl5eUlJQnJ3gvL1EZGUwsLEYUFD8WFjIQECRMTOZERM5ERMxCQsfBwcGwsLA5Oa0wMJORkZGKiopWVl1GRlZVVVVLS0sYGElISEhDQ0MSEjcdHSwjIygBAQNGSv/39/dNTepFRdPOzs7Dw8MxNMM2NqSioqIvL4+Hh4eGhoZ4eHhzc3MlJXFwcHBqamphYWJRUVEqKlE9PVA2Nk81NUwoJ0AmJjkuLjYRETMVFSz5+fm4uLivr6+hi5Bvb29nZ2clJVNBQUwgIEwyMkktLS8LCxYVEAP/2QL09PRfXO/r6+uZhaG2mG5fX19OTlpXV1gjI1gfH1VGRko9PUkrKz4YGDcXFyL/1QFRUfZrZds2OdDMzMw2OMk8PLcsLFZDQ1AxMUAFByqRdiWcfQb/zgDrvABbWfVlYOR0a9eJer6RgLCqkIAjI0DbszRwXTOTeTCSeCuJcCaFbCQGBiIfHyDxwhG2kg//5ABZWPg+Qd05PNeFeNODdbmahIccIIZfX2gTFmPFo1bKp018ZSxNPx31wwHwwAF8YwHFmwA8LwA9lAY6AAAPjklEQVR42u2cBVQcVxSG7zxZ32UhELQQHArEcCsQNDjEIEicAElD3K2xNm5t07RJmqZJmzRWd3d3d3d3b8eWt1Mqp2Sgr+fkO8vOTDkz/L1z33333jcTOMlJTnISfTEA+E7I94DuMG7UeQBG6F188x+ov2kCdIczty8e6gG9y4ht6WdMHQLdIyt79UNFY6D3OHVTSFL4sV3ddqWhdaY1W6+UdnuFucdX56xYOgK6T34FOnfj0N4wsQHg4rKqnNRtp57QNeKa/B3pW4f3goUHlZZgn/jGE7XNuKUzcmqbRkJPc87mAgepu2TQCVvm1IcvyAmruBB6mA0ZOc6pF+kxVLxubPWxhhRCj1KU4JPUNBd0wa883S5UxEEPsiebOrfE6TNKjODXMTEncmlUTw06A4xcSEjFSP2u5/XhjJzx23x7SvDYpVWWkLN0vWKT09G6HnoGvxtn2NM89TSGEYYUW2jdKOgR1u/OCdiq982rHO+wLl6lf7pmgHn1mBbMA52ZUIdxwMNRoDs7m0Jp1aJg0BlDeQqxxDT6gc4M3pZLcW1/0J3CNCw44i8GnemfbhFw0FDQnREl2CpYl+h84+LKKDLjiReD7kQ0YKuJpus8L3uOx2bUI3qDRb0Iz9A5CJfGYCRe9S7QnbHXS3pXeIKuDMkQvYxElvuB3sydj81mHFMKujI8m5oEIiwcBnqzfjpGVpyhcyLsu8RkFpAlVfcA4X26lQgmfMp5oC/9gyhCRKjw1nk+LpeuKzx6HHRmVYldQAhPvzFR33GRTcXLOloOgM74Hk3BAjLT1R3eAAajDhOx0SDKrbASJJDQLfonELPrqCAgAc88lrUS9OG8/m1hGInDomU/6E9lELUiZKXOtOuPFgaf8P3y3lPZMBETs3jF2q0RoDcGSH44kJpFE2NKqjMWP3TZ2sK5E4ad5xEdnOw1eIzfoD+95Zf7+Y4ZfHuiV3JEcFT0qR5PrBx+5ezJhfvXrltU0ZbRR6AECWYS2rRT10HMWhx9LAJCSBAIxVU7xs/JKM6+79pLFxwaffC6+zf091S4RPoR6S+yYdP911133cHRhw4tWLDg0muvva+uJCEzfU7e+NwaYqFEkK6G0cbJ0DPsfDiPSv4mIJNJwNRhd5BQZ1h4kn91QE1KYB9GoMKOlNramoDqyEj/pHPDq8LCnDZi8bHbLZRibLaZpJtFLP5b8qGn8KqMXaaYWDSybGgZ/E8QhiAoJytbSmOWxkHPYZx3PMNJMUGCivxn1Q2SdlQp8r5yrP7/qcfirvofCabWvIXl3tBzGAAGX721YAe1E5tJMTGT3RWXfje9yjnIaiN2kpI+zdMbepzBeyqzW2opJaot2RdistEAl1r2o9hcdiNMA9JDNhdGQc9jBPAbl/Xg1MwzrJhiATG98rcK6sd2UadW6UMoJUkXlB3J9768N1ddTh1Vfjghxt/iICYzctlX2sjbAbe9MbCvvC//TvmYrSYB+wjL0+5tHDoMep/o2QcO16dVY0xUu7LvAbd99Eo/eY+ZWQojj86cv/D+UcPhP8BolALcyqJpU9dUWzBCmjE24OXmO88e4IoWimObVsc3LHlmV7SfdLIB/jPGZt3YGi5IuPxCsu9pj1x1dl/pUPVdMy72nOQBXGDYlkG1Ma3fKy82v/jl2YLQKdiKF8YBDxgB5l4fqsYrhFS9j512yy0fPTZQiXHyL4j/8REARvjP8VvfFoo7Y6+87dvv1Y9/eemTt4QBsnWVIEFTjl0I/z1xHa3q/NypWdT7xXvbb/74kzslF1YNj8w4tG5Tb8Uxg8FoVGa34LHjVs2afea8K709Es8ZM+Jo/Q5KzIjNY4re1975+tvtL9zyGIsRgigYxyze4D040WPcrLOyRs0bGTfOI8JXDTYG0J3kKyYXlR68ti123/yCjMx92WWXeU5py6XElet0ZjKy3sdzxt/061cDFa2uX2A8ceOhQyGx83fPWZMZnxAb0v5Mada8lX6gM2O855Y2LorNCFpRE07sdruPw8fuMAWcUYuxic3Erj3Ut9/r755P6bk3Hx5ABPc0wmy1CAHV4hUcFot4CR8SnjJ9Yua9R8svuiJRDy+4Pdh7wryidW9WhBQEVZsIphTLKaysj0iZrbjL9Cq6VL0W5HjyuUjMsgolVhCMSWfOSbB0ScG5vKWu4vDas0aM84726nYIuXzypgcW1Bdn5K1Yhi2iUDdX1IwuTZ4m03fgHc+LegUS6ZR1Mb1dTlY2BFNcHbN7X2zbpaM3D+mmL19ZFhkZ3tdht2BispmsLOUWcWUy2qTddaTqFY3JVP3tyWarzUSo3Ydaq5ZVL1zVzR7fYpsPIa5w5J4aKgeuD7PaH+0r/NuTCSGUTu1u0BvZFEMxUf6MxkSCFpczsPgw8Kr3RL2yxH9zMkKEmrOzoLsEb46NtFgEm5n5YOcdlYoaJI04k9mlwqW376f9rnr/SarxbBmrlRBBqoy1maZqXpON2K1zju+FE2Des7EXhFqINmS5ihqLqXZFbhIl6oTbWUP0G3i2pBexAkMda5gERIYLFowJs3OnYxBKA1sXXxN9gvXlFddsKZluJpRiN48TD4WqvNhji9atmxZSg83uyUO/ga++ccedPzxOVfns3uC8isPtpx9ry5zpxJSwU+TwIATsnvrsnid0SIn8dm0+XJyae67DR+rJEIKpj7XPmvtuLI27FUQmHI/ELJj1HXjb96c1Nze//Thl5kWKAas/lArMW1cO2XBk38wqH4qJ2UykvktoSlB8e2VhNOhFcFyhZ3tdQUvMGUnLAmeujt94/4XDzwGVMwsoSyYHvPzWI6ed9mLz259TVjsrW5pwJqh4zV7fUJyXG4kE/9zV6cUbryuapFtlr7Z7jck75xauHT1t2mV3H5gkFzWD1Lg+oRgzxz77qubTRJp//Awr/sx81FE2FiSU1Cki7sDBI0uWTDs4dNSIKF/lr/QOswosnZXwgIHfPSLKfeSlmwLIH6c9PNXrTyzR+8TNp4o0We+dtzQ33/LSbx886rKra74gqMIPeGB4CSEsmqa88NPPL9y8vcRiY94r7+GaGwzAA8OyEWF3vd9zN9+8ffvCUCVzY+ZFNOYu4ILoRU4WTM045bntS1sIEfe1LUnHmrOAD/qfgVkajLDgNCslkkusssXFI4APLszALnnybMXcWdUsSSbOdk7aJeDRgE0sQZT1sb6Dy864zybgAwMcrWEZEUvUmIvIfp3Ji/saYG+6hYl1L5SZP1jx6VHAC971lOnU0OkOJOVB4CP8ioxZFMA6qsx33fJ15Mjcw49eyLqAuplXU0ApuwSX8RIdlKf0iCxNmzN0ZkFSdHjgHOCHW4+Gk86ZgcVdVrjTtFHAD4OgNAizspONO1bs3Tcc+MEAw9uoTZHJ7MtaqYj4NwJXGJaYzci95mVjTw6+qfthEPDEZcsxErTxgeWTJlqyiqNoJnF1qsUl0BV5Wfy10ZDzONN7zUTqEsvs65rcbLR+JWd6F+0gilIGs7IVt+7hYWmIYTxmUrsMbLyx4s1MZmzgabwZwGMqtf0x6LJgjEh4I0/TmxFGJlCT0rfUxF+W81w7DPjBCEWp2Or+PIwCi224YBLwwyDYtIIgpJnftHppKk/5gwEqkyS9LAIzvcohDboIOMK3MowI2uDAjmS944cCRwTf4CTa2Iu09TxdsZ6n+Dv2aRszK4sP7ADn3n058IPH0zbCwgJ7xEjRzZ8/RC8IJepwYzrd8wmatxc4IkL0X+EPLuCesAt0zWTgBzGe+bN4xgIxAxfMBn4YBJ5PYTNiz3J2WdM037sT+MEIkzOpSbUn27D5jVQ1+gI/GCBKys9cNbzb4rZqXTKdq3xS5MEAom35qvtqvZl5Fl/5Olw0x+Hq7XX1BxvdyFv9dkWJD8tvNP0HaRP2EHBGRLvSkdLOcmqpQYPWAm+UTsduJtW6hT1+JPDGmZmYidSGNdKXq2aqwth24iZT8ww4rnmAp+ir0uFk9tXmwXQGJyubmimuPJdontdhczKeUwi8YYCLUzHLF5Rvl954nopjl978FqxJ1tmbGaSEl5VYhhHuiXHpFVh+puidz184M0JlIHFfjkcs78GZZwJ3eB0xa1N0lqzhiVcDd4zch5EmRWfVHA6YMhh44xrRfRma/g7B9d7AGV43+GNNPe+2Oo/srTxVmzJPtBHCCnrlp1M8nbEOOGPWfHuXxRb2kGjoEuCMu/Io0ixbuBVz0vrQMM7qiyM1nQ0/1odyubIVp4/iq35L3ILVBRcWH9iBGT91D0/1sQHG1VMbcwK1MGbKSUA5T/Y1wsj52KRpqSPNNEfCKnnK2I2Q1YKtLttq8x15Q2yX8lQRDZKSM7Pb5KZ83N+Ji50A/GCEzTsI6hIf3PTjeJ76k3B5YxVxE9ilXyLQ3VyllLdPCSOCdrxpU2Camg8ckSjqZXNw1/Uh3tbfvKY4Na82Mf91rbc8dQ1wBFu/kOgafwUaUwQcEXyZk60fs8Ar4fKHi4EjZH9wN7D2/V7expsBGpcRJrXLeBMEnDAL+MEIV6f98fkSbRM4bFoE8IMBkpf6U+IyraqXxQti2beXp/xMZNYHQSZMsRLVWD9KeQM2KbYceMOj/5sJQcuw3SKKVsDU4kNNSbV5CUd4eXRdQ/SuAw1lsfEtMwMD+yxfvjw3KC0+NmRj++irZ3kBf8ivWiVGDZ+UdfeUKVNGi6zdO+kKjwgvX+Din1H4n2OM8jhVxkNGPIiOigqOiEhOBC5J7lh8/SkaGhpOl3hoifhG4hDuPDj5UqcFa6E+Dmlj9Q8cH9/BW4MyecEyYtKAkwomEpvJhAjB9uJZnPV3Eg9FYk3bjKDT8/unU/VlnDTO+jtg8OyDEWJvkplpaikEl2GiPP+behFneqFoNUXkXH+pqSpBhKZoiFoYSiRj22hxHGf+ALtKMHG231SjPHdE7AlFABHXhxG5P4m33A6cYewIx/7r8hOoServJWWX3yoFjXAiOTVe/ixwhhH2tjrCnl55CrUh0S+aZsuDcIE/FkQcCbuAOyK2EOFe7wZiFQjOngQSg0cHSEEDh09LBv4YmmbP9l4UJuqbvhlkfJ+pxaI70Dn7gUO8tjoThh0MIAiHjAAZv3WBkj+Ebo0GHpkUErJz7RmYRHaowesc+c1ky+79vAVflcnbBn8zk9K0IlWv0TMXI+xs4jRHA/CFoUGUlo0FBcM906lAY4cAv+zPy0l50AAqB2ZSxxxe3pP+izdPc+KZPS9Mc7Ru8+JtKnYn/wL/jjHgYtjikFI/nuXCkDX1q4Cx6krgWi7MvSkL/k9M5uzF0n/COxhOcpKTnOQ/4XfHwtY5KDsGqwAAAABJRU5ErkJggg==";
    var newCover = $(".wid-now-playing-image:first").clone().addClass("in").children(
        "img").attr({
        "src": src,
        "onerror": "errorAlbumImg()"
    }).parent();
    $(".wid-now-playing-image:first").after(newCover).addClass("out").emulateTransitionEnd(
        750).one("bsTransitionEnd", function () {
        $(this).remove();
    });
    setTimeout(function () {
        $(".wid-now-playing-image.in").removeClass("in");
    }, 17); // 1000 / 60 (fps) = 16.666666666...
}

var utcTime = false;

function initClock() {
    $("#wid-time-clock").flipcountdown({
        speedFlip: 30,
        size: (window.innerWidth < 822) ? "sm" : "md",
        tick: function () {
            date = new Date();
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date
                .getUTCDate(), date.getUTCHours(), date.getUTCMinutes(),
                date.getUTCSeconds(), date.getUTCMilliseconds());
        },
        showSecond: false
    }).click(function () {
        utcTime = !utcTime;
        setUTCToLocalClock((window.innerWidth < 822) ? true : undefined);
    });
    $(".xdsoft_digit.xdsoft_separator").addClass("fade");
    setInterval(function () {
        $(".xdsoft_digit.xdsoft_separator").toggleClass("in");
    }, 1000);
}

function checkForUTCToLocalClockSize() {
    if (window.innerWidth < 822) {
        if ($("#wid-time-clock .xdsoft_flipcountdown").hasClass("xdsoft_size_md"))
            setUTCToLocalClock(true);
    } else {
        if ($("#wid-time-clock .xdsoft_flipcountdown").hasClass("xdsoft_size_sm"))
            setUTCToLocalClock();
    }
}

function setUTCToLocalClock(smallClock) {
    var date = new Date();
    if (($("#wid-time-clock .xdsoft_flipcountdown").hasClass("xdsoft_size_md") &&
            smallClock) || ($("#wid-time-clock .xdsoft_flipcountdown").hasClass(
            "xdsoft_size_sm") && smallClock === undefined)) {
        $("#wid-time-clock").empty().removeData();
    }
    if (utcTime) {
        $("#wid-time-clock").flipcountdown({
            speedFlip: 30,
            size: smallClock ? "sm" : "md",
            tick: function () {
                return date;
            },
            showSecond: false
        }).attr("title", "Clic para ver la hora UTC");
        var tz = String(String(date).split("(")[1]).split(")")[0];
        // Si no hay nombre de zona horaria definida por el sistema, entonces
        // se devuelve un nombre de zona horaria genérico
        tz = (tz === undefined) ? "Hora local" : tz;
        $("#wid-time-zone").html($("<span/>").attr({
            class: "wid-clock-timezone-name",
            title: tz
        }).html(tz));
    } else {
        $("#wid-time-clock").flipcountdown({
            speedFlip: 30,
            size: smallClock ? "sm" : "md",
            tick: function () {
                return new Date(date.getUTCFullYear(), date.getUTCMonth(),
                    date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(),
                    date.getUTCSeconds(), date.getUTCMilliseconds());
            },
            showSecond: false
        }).attr("title", "Clic para ver la hora local");
        $("#wid-time-zone").html($("<a/>").attr({
            href: "https://es.wikipedia.org/wiki/Tiempo_universal_coordinado",
            class: "wid-clock-timezone-name",
            title: "Tiempo Universal Coordinado",
            target: "_blank"
        }).text("UTC"));
    }
}

function initWidget() {
    $("#wid-now-playing-live-indicator").fadeOut(0);
    $("#wid-about-button").hover(function () {
        $(this).addClass("btn-info").removeClass("btn-link");
    }, function () {
        $(this).removeClass("btn-info").addClass("btn-link");
    }).click(function () {
        $("#wid-sidebar-info").collapse("hide");
        $("#wid-sidebar-about").collapse("show");
    });
    $("#wid-sidebar-about-close").click(function () {
        $("#wid-sidebar-info").collapse("show");
        $("#wid-sidebar-about").collapse("hide");
    });
    new UniversalHTML5Audio({
        selector: "radiognu",
        source_message: "Selecciona señal",
        sources: [{
            name: 'Normal',
            url: 'http://audio.radiognu.org/radiognu.ogg'
        }, {
            name: 'Liviana',
            url: 'http://audio.radiognu.org/radiognu2.ogg'
        }, {
            name: 'AM',
            url: 'http://audio.radiognu.org/radiognuam.ogg'
        }]
    });
}

function startWidget() {
    $("#wid-loading-message-text").html("<i class='icon-ok'></i> ¡Conectado!");
    $("#wid-now-playing-info").removeClass("hide");
    $("#wid-loading-message").animate({
        "opacity": "0"
    }, 1000, function () {
        $(this).css("display", "none").remove();
        $("iframe", window.parent.document).height($("#radiognu-widget").height());
    });
}

$(document).ready(function () {
    titl = document.title;
    initWidget();
    initSocket();
    getInfo();
    initClock();
    formatTitle();
    setInterval(formatTitle, 1000);
    // Verificando si el usuario está usando la combinación que nos odia
    if (navigator.userAgent.indexOf("Android") >= 0 && navigator.userAgent.indexOf(
            "Chrome") >= 0) {
        $("#chrome-in-android-bad-combo").modal("show");
    } else {
        // En caso contrario, no necesitamos el modal
        $("#chrome-in-android-bad-combo").remove();
    }
});

var updateTimeout = false,
    scheduleUpdate = function (delay) {
        clearTimeout(updateTimeout);
        updateTimeout = setTimeout(function () {
            try {
                window.applicationCache.update();
            } catch (ex) {
                console.log('appCache.update: ' + ex);
            }
        }, delay || 300000);
    };

scheduleUpdate(3000);

$(window).load(function () {
    // Verifica si hay un nuevo appCache disponible
    window.applicationCache.addEventListener('updateready', function (e) {
        if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
            $("#chrome-in-android-bad-combo").modal("hide");
            $("#new-version-available").modal("show");
            scheduleUpdate();
        }
    }, false);
    window.applicationCache.addEventListener('noupdate', function () {
        scheduleUpdate();
    }, false);
    window.applicationCache.addEventListener('error', function () {
        scheduleUpdate();
    }, false);
    formatTitle();
    $("iframe", window.parent.document).height($("#radiognu-widget").height());
    // $(".wid-title").hoverForMore({
    //     speed: 50.0,
    //     removeTitle: false
    // });
}).resize(function () {
    checkForUTCToLocalClockSize();
    $("iframe", window.parent.document).height($("#radiognu-widget").height());
});
