var audio;

function displayData() {
    if (typeof audio.mozGetMetadata === 'function') {
        var tags = audio.mozGetMetadata();
        var html = ''
        for (tag in tags) {
            if (tag === 'TITLE' || tag === 'ARTIST') {
                html += '<p><strong>' + tag + ': ' + tags[tag] + '</strong></p>\n';
            } else {
                html += '<p>' + tag + ': ' + tags[tag] + '</p>\n';
            }
        }
        console.log('request');
        var request = new XMLHttpRequest();
        request.open('GET', 'http://api.radiognu.org/?img_size=200&artist=' + tags[
            'ARTIST'] + '&title=' + tags['TITLE'], true);
        request.onloadstart = function (e) {
            document.getElementById('api-display').innerHTML =
                '<p><em><b>Esperando respuesta de API</b></em></p>';
        };
        request.onload = function () {
            if (this.status >= 200 && this.status < 400) {
                var data = JSON.parse(this.response),
                    html = '';
                for (item in data) {
                    if (item === 'cover') {
                        html += '<img src="' + data[item] +
                            '" alt="cover" class="cover"/>';
                    } else if (item === 'license') {
                        html += '<p>' + item + ': ' + data[item]['name'] + '</p>\n';
                    } else {
                        html += '<p>' + item + ': ' + (data[item] === '' ?
                            'Sin datos' : data[item]) + '</p>\n';
                    }
                }
                document.getElementById('api-display').innerHTML = html;
            } else {
                console.error("error");
            }
        };
        request.onerror = function () {
            console.error("ERROR");
        };
        request.send();
        document.getElementById('metadata-display').innerHTML = html;
    }
}

window.onload = function () {
    console.log('loaded');
    audio = document.getElementById('player');
    if (typeof audio.mozGetMetadata !== 'function') {
        document.getElementById('metadata-display').innerHTML =
            'Lo sentimos, este navegador no soporta <code>mozGetMetadata()</code>';
        document.getElementById('api-display').innerHTML =
            'Lo sentimos, este navegador no soporta <code>mozGetMetadata()</code>';
    }
}
